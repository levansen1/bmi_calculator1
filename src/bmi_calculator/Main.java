/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package bmi_calculator;

/**
 *
 * @author levansen
 */
public class Main {
    public static void main(String[] args){
        
        Person person1 = new Person("Alexa", 1.72, 82);
        Person person2 = new Person("Liam", 1.78, 69);
       //calculate BMI for the above conditions
       //Default values for parameters are meters and kilos
      
 System.out.println("BMI of " +person1.getName() +" is " + person1.calculateBMI());
 System.out.println("BMI of " +person2.getName() +" is " + person2.calculateBMI());
 
        //
        
        
        
    }
}
