/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package bmi_calculator;



public class Person {
     private String name;
     private double height; 
     private double weight;
   
    
      //default constructor
     public Person(){
         
     }
//constructor with parameters
    public Person(String name, double height, double weight) {
        this.name = name;
        this.height = height;//metres
        this.weight = weight;//kilos
    }
    
     
    public String getName() {
        return name;
    }

    public double getWeight() {
        return weight;
    }
    //gets weight in lbs
    public double getWeightlbs() {
        return this.weight/0.454;
    }
 
   
    public double getHeight() {
        return height;
    }
    //gets height in centimetres
        public double getHeightcms() {
       return this.height = height*100;
    }
        public double getHeightInches(){
            return this.height / 0.254;
        }

    public void setName(String name) {
        this.name = name;
    }

    public void setWeight(double Weight) {
        this.weight = Weight;
    }
    
 
    public void setHeight(double height) {
        this.height = height;
    }
    
    
    
    public void calculateBMI(){
       double bmi = weight/Math.pow(height,2);
        System.out.println(bmi);
    }
     
}















